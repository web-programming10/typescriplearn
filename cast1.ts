let x: unknown = 'hello';
let num: string = '80';
console.log((x as string).length);
console.log((<string>x).length);
console.log(((num as unknown) as number));